local main_menu_id = "hudlist_options_menu"

--Populate options menus
Hooks:Add("MenuManagerPopulateCustomMenus", "MenuManagerPopulateCustomMenus_HUDList", function(menu_manager, nodes)
	if not GameInfoManager then return end
	
	local function change_setting(setting, value)
		HUDListManager.change_setting(setting, value)
	end
	
	local function change_ignored_buff_settings(buff_id, value)
		HUDListManager.change_ignore_buff_setting(buff_id, value)
	end
	
	local function changed_ignored_special_pickup_settings(pickup_id, value)
		HUDListManager.change_ignore_special_pickup_setting(pickup_id, value)
	end
	
	MenuHelper:NewMenu(main_menu_id)
	
	MenuHelper:NewMenu("hudlist_left_list_options_menu")
	
	MenuHelper:NewMenu("hudlist_right_list_options_menu")
	MenuHelper:NewMenu("hudlist_ignore_special_pickups_menu")
	
	MenuHelper:NewMenu("hudlist_buff_list_options_menu")
	
	local left_list = {
		{ "left_list_y", "slider", { min = 0, max = 1000, step = 1, round = true } },
		{ "left_list_scale", "slider", { min = 0.5, max = 2, step = 0.05 } },
		{ "divider", "divider", { size = 12 } },
		{ "show_ammo_bags", "multichoice", { "hudlist_option_off", "hudlist_option_all", "hudlist_option_aggregate" }, "deployables" },
		{ "show_doc_bags", "multichoice", { "hudlist_option_off", "hudlist_option_all", "hudlist_option_aggregate" }, "deployables" },
		{ "show_body_bags", "multichoice", { "hudlist_option_off", "hudlist_option_all", "hudlist_option_aggregate" }, "deployables" },
		{ "show_grenade_crates", "multichoice", { "hudlist_option_off", "hudlist_option_all", "hudlist_option_aggregate" }, "deployables" },
		{ "show_sentries", "multichoice", { "hudlist_option_off", "hudlist_option_all", "hudlist_option_player_only" }, "sentries" },
		{ "show_minions", "multichoice", { "hudlist_option_off", "hudlist_option_all", "hudlist_option_player_only" }, "units" },
		{ "show_timers", "toggle", false, "timers" },
		{ "show_ecms", "toggle", false, "ecms" },
		{ "show_ecm_retrigger", "toggle", false, "ecms" },
		{ "show_pagers", "toggle", false, "pagers" },
		{ "show_tape_loop", "toggle", false, "cameras" },
	}
	
	local right_list = {
		{ "right_list_y", "slider", { min = 0, max = 1000, step = 1, round = true } },
		{ "right_list_scale", "slider", {  min = 0.5, max = 2, step = 0.05 } },
		{ "divider", "divider", { size = 12 } },
		{ "show_loot", "multichoice", { "hudlist_option_off", "hudlist_option_all", "hudlist_option_aggregate" }, "loot" },
		{ "separate_bagged_loot", "toggle", false, "loot" },
		{ "show_enemies", "multichoice", { "hudlist_option_off", "hudlist_option_all", "hudlist_option_aggregate" }, "units" },
		{ "show_hostages", "multichoice", { "hudlist_option_off", "hudlist_option_all", "hudlist_option_aggregate" }, "units" },
		{ "show_civilians", "toggle", false, "units" },
		{ "show_turrets", "toggle", false, "units" },
		{ "show_minion_count", "toggle", false, "units" },
		{ "show_pager_count", "toggle", false, "pagers" },
		{ "show_camera_count", "toggle", false, "cameras" },
		{ "show_body_count", "toggle", false, "loot" },
		{ "show_special_pickups", "toggle", false, "pickups" },
	}
	
	local ignore_special_pickups = {
		"crowbar",
		"keycard",
		"courier",
		"planks",
		"meth_ingredients",
		"secret_item",
	}
	
	local buff_list = {
		{ "buff_list_y", "slider", { min = 0, max = 1000, step = 1, round = true } },
		{ "buff_list_scale", "slider", { min = 0.5, max = 2, step = 0.05 } },
		{ "show_buffs", "toggle" },
		{ "divider", "divider", { size = 12 } },
	}
	
	local ignore_buffs = {
		--Buffs
		"aggressive_reload_aced",
		"ammo_efficiency",
		"armor_break_invulnerable",
		"berserker",
		"biker",
		"bloodthirst_aced",
		"bloodthirst_basic",
		"bullet_storm",
		"sixth_sense",
		"close_contact",
		"combat_medic",
		"combat_medic_passive",
		"desperado",
		"die_hard",
		"dire_need",
		"grinder",
		"hostage_situation",
		"hostage_taker",
		"melee_stack_damage",
		"chico_injector",
		"inspire",
		"maniac",
		"messiah",
		"overdog",
		"overkill",
		"painkiller",
		"partner_in_crime",
		"running_from_death",
		"quick_fix",
		"second_wind",
		"lock_n_load",
		"swan_song",
		"tooth_and_claw",
		"trigger_happy",
		"underdog",
		"unseen_strike",
		"up_you_go",
		"uppers",
		"yakuza",
		--Debuffs
		"anarchist_armor_recovery_debuff",
		"ammo_give_out_debuff",
		"armor_break_invulnerable_debuff",
		"bullseye_debuff",
		"grinder_debuff",
		"chico_injector_debuff",
		"inspire_debuff",
		"inspire_revive_debuff",
		"life_drain_debuff",
		"medical_supplies_debuff",
		"unseen_strike_debuff",
		"uppers_debuff",
		"sociopath_debuff",
		--Team buffs
		"armorer",
		"bulletproof",
		"crew_chief",
		"endurance",
		"forced_friendship",
		--Composite buffs
		"damage_increase",
		"damage_reduction",
		"melee_damage_increase",
		--Player actions
		"anarchist_armor_regeneration",
		"standard_armor_regeneration",
		"melee_charge",
		"reload",
		"interact",
	}
	
	
	local main = {
		hudlist_left_list_options_menu = left_list,
		hudlist_right_list_options_menu = right_list,
		hudlist_buff_list_options_menu = buff_list,
	}
	
	for menu_id, items in pairs(main) do
		for i, data in ipairs(items) do
			local id = data[1]
			local item_type = data[2]
			local item_data = data[3]
			local gim_plugin_req = data[4]
			local clbk_id = id .. "_clbk"
			
			if item_type == "toggle" then
				MenuHelper:AddToggle({
					id = id,
					title = id .. "_title",
					desc = id .. "_desc",
					callback = clbk_id,
					value = HUDListManager.ListOptions[id] and true or false,
					menu_id = menu_id,
					priority = -i,
				})
				
				MenuCallbackHandler[clbk_id] = function(self, item)
					change_setting(id, item:value() == "on")
				end
			elseif item_type == "slider" then
				MenuHelper:AddSlider({
					id = id,
					title = id .. "_title",
					desc = id .. "_desc",
					callback = clbk_id,
					value = HUDListManager.ListOptions[id],
					min = item_data.min,
					max = item_data.max,
					step = item_data.step,
					show_value = true,
					menu_id = menu_id,
					priority = -i,
				})
				
				MenuCallbackHandler[clbk_id] = function(self, item)
					if item_data.round then
						item:set_value(math.round(item:value()))
					end
					
					change_setting(id, item:value())
				end
			elseif item_type == "multichoice" then
				MenuHelper:AddMultipleChoice({
					id = id,
					title = id .. "_title",
					desc = id .. "_desc",
					callback = clbk_id,
					items = item_data,
					value = HUDListManager.ListOptions[id] + 1,
					menu_id = menu_id,
					priority = -i,
				})
				
				MenuCallbackHandler[clbk_id] = function(self, item)
					change_setting(id, item:value() - 1)
				end
			elseif item_type == "divider" then
				MenuHelper:AddDivider({
					id = id,
					size = item_data.size,
					menu_id = menu_id,
					priority = -i,
				})
			end
			
			if gim_plugin_req then
				local enabled_clbk = id .. "_enabled_clbk"
				
				MenuCallbackHandler[enabled_clbk] = function(self, item)
					return GameInfoManager and GameInfoManager.plugin_active(gim_plugin_req)
				end
				
				local menu = MenuHelper:GetMenu(menu_id)
				for i, item in pairs(menu._items_list) do
					if item:parameters().name == id then
						item._enabled_callback_name_list = { enabled_clbk }
						break
					end
				end
			end
		end
	end
	
	for i, buff_id in ipairs(ignore_buffs) do
		local id = "ignore_buff_" .. buff_id
		local clbk_id = id .. "_clbk"
		
		MenuHelper:AddToggle({
			id = id,
			title = id .. "_title",
			desc = id .. "_desc",
			callback = clbk_id,
			value = not (HUDListManager.ListOptions.ignore_buffs[buff_id] and true or false),
			menu_id = "hudlist_buff_list_options_menu",
			priority = -(i + #buff_list),
		})
		
		MenuCallbackHandler[clbk_id] = function(self, item)
			change_ignored_buff_settings(buff_id, item:value() ~= "on")
		end
	end
	
	for i, pickup_id in ipairs(ignore_special_pickups) do
		local id = "ignore_special_pickup_" .. pickup_id
		local clbk_id = id .. "_clbk"
		
		MenuHelper:AddToggle({
			id = id,
			title = id .. "_title",
			desc = id .. "_desc",
			callback = clbk_id,
			value = not (HUDListManager.ListOptions.ignore_special_pickups[pickup_id] and true or false),
			menu_id = "hudlist_ignore_special_pickups_menu",
			priority = -i,
		})
		
		MenuCallbackHandler[clbk_id] = function(self, item)
			changed_ignored_special_pickup_settings(pickup_id, item:value() ~= "on")
		end
	end
end)

Hooks:Add("MenuManagerBuildCustomMenus", "MenuManagerBuildCustomMenus_HUDList", function(menu_manager, nodes)
	if not GameInfoManager then return end

	local back_clbk = "hudlist_back_clbk"
	
	MenuCallbackHandler[back_clbk] = function(self, item)
		HUDListMenu.save_settings()
	end
	
	nodes[main_menu_id] = MenuHelper:BuildMenu(main_menu_id)
	MenuHelper:AddMenuItem(nodes.lua_mod_options_menu, main_menu_id, main_menu_id .. "_title", main_menu_id .. "_desc" )
	
	nodes["hudlist_left_list_options_menu"] = MenuHelper:BuildMenu("hudlist_left_list_options_menu", { back_callback = back_clbk })
	MenuHelper:AddMenuItem(nodes[main_menu_id], "hudlist_left_list_options_menu", "hudlist_left_list_options_menu_title", "hudlist_left_list_options_menu_desc" )
	
	nodes["hudlist_right_list_options_menu"] = MenuHelper:BuildMenu("hudlist_right_list_options_menu", { back_callback = back_clbk })
	MenuHelper:AddMenuItem(nodes[main_menu_id], "hudlist_right_list_options_menu", "hudlist_right_list_options_menu_title", "hudlist_right_list_options_menu_desc" )
	
	if GameInfoManager and GameInfoManager.has_plugin("pickups") then
		nodes["hudlist_ignore_special_pickups_menu"] = MenuHelper:BuildMenu("hudlist_ignore_special_pickups_menu")
		MenuHelper:AddMenuItem(nodes["hudlist_right_list_options_menu"], "hudlist_ignore_special_pickups_menu", "hudlist_ignore_special_pickups_menu_title", "hudlist_ignore_special_pickups_menu_desc" )
	end
	
	if GameInfoManager and GameInfoManager.has_plugin("buffs") then
		nodes["hudlist_buff_list_options_menu"] = MenuHelper:BuildMenu("hudlist_buff_list_options_menu", { back_callback = back_clbk })
		MenuHelper:AddMenuItem(nodes[main_menu_id], "hudlist_buff_list_options_menu", "hudlist_buff_list_options_menu_title", "hudlist_buff_list_options_menu_desc" )
	end
end)

Hooks:Add("LocalizationManagerPostInit", "LocalizationManagerPostInit_HUDList_localization", function(self)
	local strings = {
		hudlist_options_menu_title = "HUDList Options",
		hudlist_options_menu_desc = "Options for HUDList general behavior and content",
		hudlist_left_list_options_menu_title = "Left List Options",
		hudlist_left_list_options_menu_desc = "Options for the left-hand list, displaying various informaton such as timers and equipment",
		hudlist_right_list_options_menu_title = "Right List Options",
		hudlist_right_list_options_menu_desc = "Options for the right-hand list, displaying various counters",
		hudlist_ignore_special_pickups_menu_title = "Special Pickup Options",
		hudlist_ignore_special_pickups_menu_desc = "Allows enabling/disabling showing specific types of pickups",
		hudlist_buff_list_options_menu_title = "Buff List Options",
		hudlist_buff_list_options_menu_desc = "Options for the list displaying player buff/debuffs and other timed effects",
		
		right_list_y_title = "Offset",
		right_list_y_desc = "Position offset from top of the HUD.",
		left_list_y_title = "Offset",
		left_list_y_desc = "Position offset from top of the HUD.",
		buff_list_y_title = "Offset",
		buff_list_y_desc = "Position offset from bottom of the HUD.",
		right_list_scale_title = "Scale",
		right_list_scale_desc = "Size scale of the right list.",
		left_list_scale_title = "Scale",
		left_list_scale_desc = "Size scale of the left list.",
		buff_list_scale_title = "Scale",
		buff_list_scale_desc = "Size scale of the buff list.",
		show_timers_title = "Timers",
		show_timers_desc = "Show drill, saw and hacking timers etc.",
		show_ammo_bags_title = "Ammo Bags",
		show_ammo_bags_desc = "Show deployed ammo bags and shelves. Aggregate option merges all sources into one item.",
		show_doc_bags_title = "Doctor Bags",
		show_doc_bags_desc = "Show deployed doctor bags and cabinets. Aggregate option merges all sources into one item.",
		show_body_bags_title = "Body Bags",
		show_body_bags_desc = "Show deployed body bag cases. Aggregate option merges all sources into one item.",
		show_grenade_crates_title = "Grenade Crates",
		show_grenade_crates_desc = "Show deployed grenade crates. Aggregate option merges all sources into one item.",
		show_sentries_title = "Sentries",
		show_sentries_desc = "Show deployed and active team and/or player sentries.",
		show_ecms_title = "ECM Jammer Timers",
		show_ecms_desc = "Show active ECM jammers and whether they block pagers.",
		show_ecm_retrigger_title = "ECM Retrigger Timers",
		show_ecm_retrigger_desc = "Show countdown until player-owned ECM feedback can be used again.",
		show_minions_title = "Jokers",
		show_minions_desc = "Show enemies converted by the team and/or player.",
		show_pagers_title = "Pager Timers",
		show_pagers_desc = "Show active pagers with time remaining, if they are answered or not, and time remaining.",
		show_tape_loop_title = "Tape-Loop Timers",
		show_tape_loop_desc = "Show timer for active camera tape-loops.",
		show_enemies_title = "Enemy Counter",
		show_enemies_desc = "Show counter for enemy units on the map. Aggregate option merges all enemy types into one item.",
		show_turrets_title = "SWAT Turret Counter",
		show_turrets_desc = "Show counter for SWAT turrets on the map.",
		show_civilians_title = "Free Civilian Counter",
		show_civilians_desc = "Show counter for number of untied civilians on the map.",
		show_hostages_title = "Hostage Counter",
		show_hostages_desc = "Show counter for current cop and civilian hostages on the map. Aggregate option merges all types into one item.",
		show_minion_count_title = "Joker Counter",
		show_minion_count_desc = "Show counter for current number of converted enemies on the map.",
		show_pager_count_title = "Pager Counter",
		show_pager_count_desc = "Show counter for number of pagers used. May be inaccurate if joining as drop-in. Auto-disabled when heist goes loud.",
		show_camera_count_title = "Camera Counter",
		show_camera_count_desc = "Show counter for number of active cameras on the map. Does not include drone cameras. Auto-disabled when heist goes loud.",
		show_body_count_title = "Body Counter",
		show_body_count_desc = "Show counter for number of dead bodies on the map. Auto-disabled when heist goes loud.",
		show_loot_title = "Loot Counter",
		show_loot_desc = "Show counter for number and types of active loot items on the map. May not show loot if not yet flagged as active or spawned. Aggregate option merges all types into one item.",
		separate_bagged_loot_title = "Separate Bagged Loot",
		separate_bagged_loot_desc = "Show separate values for bagged and unbagged loot items",
		show_special_pickups_title = "Special Pickups",
		show_special_pickups_desc = "Uncheck to disable display of all special equipment/pickusp on the map (if enabled, individual types can be enabled/disabled in submenu).",
		show_buffs_title = "Show Buffs",
		show_buffs_desc = "Uncheck to disable display of all buffs (if enabled, individual buffs can be enabled/disabled below).",

		ignore_buff_aggressive_reload_aced_title = "Aggressive Reload",
		ignore_buff_aggressive_reload_aced_desc = "",
		ignore_buff_ammo_efficiency_title = "Ammo Efficiency",
		ignore_buff_ammo_efficiency_desc = "",
		ignore_buff_armor_break_invulnerable_title = "Armor Break",
		ignore_buff_armor_break_invulnerable_desc = "",
		ignore_buff_berserker_title = "Berserker",
		ignore_buff_berserker_desc = "",
		ignore_buff_biker_title = "Prospect",
		ignore_buff_biker_desc = "",
		ignore_buff_bloodthirst_aced_title = "Bloodthirst (Aced)",
		ignore_buff_bloodthirst_aced_desc = "",
		ignore_buff_bloodthirst_basic_title = "Bloodthirst (Basic)",
		ignore_buff_bloodthirst_basic_desc = "",
		ignore_buff_bullet_storm_title = "Bullet Storm",
		ignore_buff_bullet_storm_desc = "",
		ignore_buff_sixth_sense_title = "Sixth Sense",
		ignore_buff_sixth_sense_desc = "",
		ignore_buff_close_contact_title = "Close Contact",
		ignore_buff_close_contact_desc = "",
		ignore_buff_combat_medic_title = "Combat Medic (active)",
		ignore_buff_combat_medic_desc = "",
		ignore_buff_combat_medic_passive_title = "Combat Medic (passive)",
		ignore_buff_combat_medic_passive_desc = "",
		ignore_buff_desperado_title = "Desperado",
		ignore_buff_desperado_desc = "",
		ignore_buff_die_hard_title = "Die Hard",
		ignore_buff_die_hard_desc = "",
		ignore_buff_dire_need_title = "Dire Need",
		ignore_buff_dire_need_desc = "",
		ignore_buff_grinder_title = "Grinder",
		ignore_buff_grinder_desc = "",
		ignore_buff_hostage_situation_title = "Hostage Situation",
		ignore_buff_hostage_situation_desc = "",
		ignore_buff_hostage_taker_title = "Hostage Taker",
		ignore_buff_hostage_taker_desc = "",
		ignore_buff_melee_stack_damage_title = "Infiltrator Melee Stacks",
		ignore_buff_melee_stack_damage_desc = "",
		ignore_buff_chico_injector_title = "Kingpin Injector",
		ignore_buff_chico_injector_desc = "",
		ignore_buff_inspire_title = "Inspire",
		ignore_buff_inspire_desc = "",
		ignore_buff_maniac_title = "Maniac",
		ignore_buff_maniac_desc = "",
		ignore_buff_messiah_title = "Messiah",
		ignore_buff_messiah_desc = "",
		ignore_buff_overdog_title = "Overdog",
		ignore_buff_overdog_desc = "",
		ignore_buff_overkill_title = "Overkill",
		ignore_buff_overkill_desc = "",
		ignore_buff_painkiller_title = "Painkiller",
		ignore_buff_painkiller_desc = "",
		ignore_buff_partner_in_crime_title = "Partner in Crime",
		ignore_buff_partner_in_crime_desc = "",
		ignore_buff_running_from_death_title = "Running From Death",
		ignore_buff_running_from_death_desc = "",
		ignore_buff_quick_fix_title = "Quick Fix",
		ignore_buff_quick_fix_desc = "",
		ignore_buff_second_wind_title = "Second Wind",
		ignore_buff_second_wind_desc = "",
		ignore_buff_lock_n_load_title = "Lock n Load",
		ignore_buff_lock_n_load_desc = "",
		ignore_buff_swan_song_title = "Swan Song",
		ignore_buff_swan_song_desc = "",
		ignore_buff_tooth_and_claw_title = "Tooth and Claw",
		ignore_buff_tooth_and_claw_desc = "",
		ignore_buff_trigger_happy_title = "Trigger Happy",
		ignore_buff_trigger_happy_desc = "",
		ignore_buff_underdog_title = "Underdog",
		ignore_buff_underdog_desc = "",
		ignore_buff_unseen_strike_title = "Unseen Strike",
		ignore_buff_unseen_strike_desc = "",
		ignore_buff_up_you_go_title = "Up You Go",
		ignore_buff_up_you_go_desc = "",
		ignore_buff_uppers_title = "Uppers",
		ignore_buff_uppers_desc = "",
		ignore_buff_yakuza_title = "Yakuza",
		ignore_buff_yakuza_desc = "",
		ignore_buff_anarchist_armor_recovery_debuff_title = "Lust For Life Cooldown",
		ignore_buff_anarchist_armor_recovery_debuff_desc = "",
		ignore_buff_ammo_give_out_debuff_title = "Ammo Give Out Cooldown",
		ignore_buff_ammo_give_out_debuff_desc = "",
		ignore_buff_armor_break_invulnerable_debuff_title = "Armor Break Cooldown",
		ignore_buff_armor_break_invulnerable_debuff_desc = "Composite debuff, will show as part of main buff icon even if disabled",
		ignore_buff_bullseye_debuff_title = "Bullseye Cooldown",
		ignore_buff_bullseye_debuff_desc = "",
		ignore_buff_grinder_debuff_title = "Grinder Cooldown",
		ignore_buff_grinder_debuff_desc = "Composite debuff, will show as part of main buff icon even if disabled",
		ignore_buff_chico_injector_debuff_title = "Kingpin Injector Cooldown",
		ignore_buff_chico_injector_debuff_desc = "Composite debuff, will show as part of main buff icon even if disabled",
		ignore_buff_inspire_debuff_title = "Inspire Boost Cooldown",
		ignore_buff_inspire_debuff_desc = "",
		ignore_buff_inspire_revive_debuff_title = "Inspire Revive Cooldown",
		ignore_buff_inspire_revive_debuff_desc = "",
		ignore_buff_life_drain_debuff_title = "Life Drain Cooldown",
		ignore_buff_life_drain_debuff_desc = "",
		ignore_buff_medical_supplies_debuff_title = "Medical Supplies Cooldown",
		ignore_buff_medical_supplies_debuff_desc = "",
		ignore_buff_unseen_strike_debuff_title = "Unseen Strike Cooldown",
		ignore_buff_unseen_strike_debuff_desc = "Composite debuff, will show as part of main buff icon even if disabled.",
		ignore_buff_uppers_debuff_title = "Uppers Cooldown",
		ignore_buff_uppers_debuff_desc = "Composite debuff, will show as part of main buff icon even if disabled.",
		ignore_buff_sociopath_debuff_title = "Sociopath Cooldown",
		ignore_buff_sociopath_debuff_desc = "",
		ignore_buff_armorer_title = "Armorer",
		ignore_buff_armorer_desc = "Team buff",
		ignore_buff_bulletproof_title = "Bulletproof",
		ignore_buff_bulletproof_desc = "Team buff",
		ignore_buff_crew_chief_title = "Crew Chief",
		ignore_buff_crew_chief_desc = "Team buff",
		ignore_buff_endurance_title = "Endurance",
		ignore_buff_endurance_desc = "Team buff",
		ignore_buff_forced_friendship_title = "Forced Friendship",
		ignore_buff_forced_friendship_desc = "Team buff",
		
		ignore_buff_damage_increase_title = "Damage Bonus",
		ignore_buff_damage_increase_desc = "Composite buff. Combines several damage increase buffs into a single one.",
		ignore_buff_damage_reduction_title = "Received Damage Reduction",
		ignore_buff_damage_reduction_desc = "Composite buff. Combines several damage reduction buffs into a single one.",
		ignore_buff_melee_damage_increase_title = "Melee Damage Bonus",
		ignore_buff_melee_damage_increase_desc = "Composite buff. Combines several melee damage increase buffs into a single one.",

		ignore_buff_anarchist_armor_regeneration_title = "Anarchist Armor Regeneration",
		ignore_buff_anarchist_armor_regeneration_desc = "",
		ignore_buff_standard_armor_regeneration_title = "Armor Regeneration",
		ignore_buff_standard_armor_regeneration_desc = "",
		ignore_buff_melee_charge_title = "Melee Charge",
		ignore_buff_melee_charge_desc = "",
		ignore_buff_reload_title = "Reload Duration",
		ignore_buff_reload_desc = "",
		ignore_buff_interact_title = "Interaction Duration",
		ignore_buff_interact_desc = "",

		ignore_special_pickup_crowbar_title = "Crowbars",
		ignore_special_pickup_crowbar_desc = "",
		ignore_special_pickup_keycard_title = "Keycards",
		ignore_special_pickup_keycard_desc = "",
		ignore_special_pickup_courier_title = "Gage Courier Packets",
		ignore_special_pickup_courier_desc = "",
		ignore_special_pickup_planks_title = "Planks",
		ignore_special_pickup_planks_desc = "",
		ignore_special_pickup_meth_ingredients_title = "Meth Ingredients",
		ignore_special_pickup_meth_ingredients_desc = "",
		ignore_special_pickup_secret_item_title = "Secret Items",
		ignore_special_pickup_secret_item_desc = "Various special items such as rings on Birth of Sky and bottles on Biker Heist.",
		
		hudlist_option_off = "Off",
		hudlist_option_all = "All",
		hudlist_option_aggregate = "Aggregate",
		hudlist_option_player_only = "Player only",
	}
	
	self:add_localized_strings(strings)
end)

local file_name = ModPath .. "saved_settings.json"

HUDListMenu = {
	save_settings = function()
		local file = io.open(file_name, "w+")
		if file then
			file:write(json.encode(HUDListManager.ListOptions))
			file:close()
		end
	end,
	
	load_settings = function()
		local file = io.open(file_name, "r")
		if file then
			HUDListManager.ListOptions = json.decode(file:read("*all"))
			file:close()
		else
			HUDListMenu.save_settings()
		end
	end,
}

HUDListMenu.load_settings()
