--Default settings for HUDList
HUDListManager.ListOptions = HUDListManager.ListOptions or {
	--General settings
	right_list_y = 0,	--Margin from top for the right list
	right_list_scale = 1,	--Size scale of right list
	left_list_y = 40,	--Margin from top for the left list
	left_list_scale = 1,	--Size scale of left list
	buff_list_y = 80,	--Margin from bottom for the buff list
	buff_list_scale = 1,	--Size scale of buff list

	--Left side list
	show_timers = true,	--Drills, time locks, hacking etc.
	show_ammo_bags = 2,	--Show ammo bags/shelves and remaining amount
	show_doc_bags = 2,	--Show doc bags/cabinets and remaining charges
	show_body_bags = 2,	--Show body bags and remaining amount. Auto-disabled if heist goes loud
	show_grenade_crates = 2,	--Show grenade crates with remaining amount
	show_sentries = 2,	--Deployable sentries, color-coded by owner
	show_ecms = true,	--Active ECMs with time remaining
	show_ecm_retrigger = true,	--Countdown for player owned ECM feedback retrigger delay
	show_minions = 2,	--Converted enemies, type and health
	show_pagers = true,	--Show currently active pagers
	show_tape_loop = true,	--Show active tape loop duration

	--Right side list
	show_enemies = 1,		--Currently spawned enemies
	show_turrets = true,	--Show active SWAT turrets
	show_civilians = true,	--Currently spawned, untied civs
	show_hostages = 1,	--Currently tied civilian and dominated cops
	show_minion_count = true,	--Current number of jokered enemies
	show_pager_count = true,	--Show number of triggered pagers (only counts pagers triggered while you were present). Auto-disabled if heist goes loud
	show_camera_count = true,	--Show number of active cameras on the map. Auto-disabled if heist goes loud (experimental, has some issues)
	show_body_count = true,		--Show number of corpses/body bags on map. Auto-disabled if heist goes loud
	show_loot = 1,	--Show spawned and active loot bags/piles (may not be shown if certain mission parameters has not been met)
		separate_bagged_loot = true,	 --Show bagged/unbagged loot as separate values
	show_special_pickups = true,	--Show number of special equipment/items
		ignore_special_pickups = {	--Exclude specific special pickups from showing
			crowbar = false,
			keycard = false,
			courier = false,
			planks = false,
			meth_ingredients = false,
			secret_item = false,	--Biker heist bottle / BoS rings
		},
	
	--Buff list
	show_buffs = true,	--Show active effects (buffs/debuffs)
		ignore_buffs = {	--Exclude specific effects from showing
			--Buffs
			aggressive_reload_aced = false,
			ammo_efficiency = false,
			armor_break_invulnerable = false,
			berserker = true,
			biker = false,
			bloodthirst_aced = false,
			bloodthirst_basic = true,
			bullet_storm = false,
			sixth_sense = false,
			close_contact = true,
			combat_medic = true,
			combat_medic_passive = true,
			desperado = false,
			die_hard = true,
			dire_need = false,
			grinder = false,
			hostage_situation = true,
			hostage_taker = true,
			melee_stack_damage = true,
			chico_injector = false,
			inspire = false,
			maniac = true,
			messiah = false,
			overdog = true,
			overkill = true,
			painkiller = true,
			partner_in_crime = true,
			running_from_death = false,
			quick_fix = true,
			second_wind = false,
			lock_n_load = false,
			swan_song = true,
			tooth_and_claw = false,
			trigger_happy = true,
			underdog = true,
			unseen_strike = false,
			up_you_go = true,
			uppers = false,
			yakuza = true,

			--Debuffs
			anarchist_armor_recovery_debuff = false,
			ammo_give_out_debuff = false,
			armor_break_invulnerable_debuff = true, --Composite debuff, will also show as part of the main buff icon,
			bullseye_debuff = false,
			grinder_debuff = true, --Composite debuff, will also show as part of the main buff icon,
			chico_injector_debuff = true, --Composite debuff, will also show as part of the main buff icon,
			inspire_debuff = false,
			inspire_revive_debuff = false,
			life_drain_debuff = false,
			medical_supplies_debuff = false,
			unseen_strike_debuff = true, --Composite debuff, will also show as part of the main buff icon,
			uppers_debuff = true, --Composite debuff, will also show as part of the main buff icon,
			sociopath_debuff = false,

			--Team buffs
			armorer = false,
			bulletproof = false,
			crew_chief = false,
			endurance = true,
			forced_friendship = true,

			--Composite buffs
			damage_increase = false,
			damage_reduction = false,
			melee_damage_increase = false,

			--Player actions
			anarchist_armor_regeneration = false,
			standard_armor_regeneration = false,
			melee_charge = false,
			reload = false,
			interact = false,
		},
}

function HUDListManager.change_setting(setting, value)
	if HUDListManager.ListOptions[setting] ~= value then
		HUDListManager.ListOptions[setting] = value
		
		local clbk = "_set_" .. setting
		if HUDListManager[clbk] and managers.hudlist then
			managers.hudlist[clbk](managers.hudlist)
			return true
		end
	end
end

function HUDListManager.change_ignore_buff_setting(buff, value)
	if HUDListManager.ListOptions.ignore_buffs[buff] ~= value then
		HUDListManager.ListOptions.ignore_buffs[buff] = value
		
		if managers.hudlist then
			managers.hudlist:_set_ignored_buff(buff, value)
		end
	end
end

function HUDListManager.change_ignore_special_pickup_setting(pickup, value)
	if HUDListManager.ListOptions.ignore_special_pickups[pickup] ~= value then
		HUDListManager.ListOptions.ignore_special_pickups[pickup] = value
		
		if managers.hudlist then
			managers.hudlist:_set_ignored_special_pickup(pickup, value)
		end
	end
end
